import {applyMiddleware,createStore} from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

const initState = {
    data: 'isGood'
}

const reducer = (state = initState, action) => {
    switch(action.type){
        case "test": 
            return Object.assign({},state,{...action});
        default:
            return state;
    }
}

//customer middleware
// const logger = (store) => (next) => (action) => {
// 	console.log("Logger fired", action);
// 	next(action);
// }

let store = createStore(reducer,applyMiddleware(logger,thunk));

export default store;
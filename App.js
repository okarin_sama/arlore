
{/* <script src="http://localhost:8081"></script> */}
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, StatusBar} from 'react-native';
import {Avatar, Button} from 'react-native-elements';
import store from './store'
import {connect, } from 'react-redux';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});


class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      ...this.props,

    };
   
  }

  render() {
    return (
      
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Button title='start' onPress = {() => {let x = 0; this.state.data == 'no data yet'? x=0: x = this.state.data;   this.props.isPressed(x)}}/>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Avatar large rounded onPress={()=>console.warn("here")} activeOpactiy = {0.6}/>
        <Text style={styles.instructions}>{instructions}</Text>
        <Text style={styles.welcome}>{this.state.data}</Text> 
        <StatusBar
     backgroundColor="blue"
     barStyle="light-content"
    
   />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

const stateToProps = (state) =>{
  return({
     data2: 'isGOod',
     data: 'no data yet'
  })
}
const dispatchToProps = (dispatch) =>{
  return({
    isPressed: (value) => {
      dispatch({
        type: 'test',
        data: value + 1
      })
    }
  })
}
